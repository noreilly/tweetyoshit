package org.prototype.rest;
 
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.prototype.service.WekaAnalyzer;

@Path("/tweet")
public class TweetService {
 
	@POST
    @Path("/classifiy")
	public Response savePayment(String tweet) {

       String calssification = WekaAnalyzer.analysisTweet(tweet);
 
		return Response.status(200).entity(calssification).build();
 
	}


}